const SERVER = new URL('http://localhost:3000');

const path = require('path')

// Require Provider 
const lti = require('ltijs').Provider
const Database = require('ltijs-sequelize')

const db = new Database('lti', 'root', '',
    {
        host: '127.0.0.1',
        dialect: 'mysql',
        logging: false
    })

// Setup provider
lti.setup('XXXXXXXXXXXXXXXXXXXXXXXXXX', // Key used to sign cookies and tokens
    /*{ // Database configuration
        url: 'mongodb://127.0.0.1:27017/lti',
        //connection: { user: 'user', pass: 'password' }
    },*/
    {
        plugin: db // Passing db object to plugin field
    },
    { // Options
        appRoute: '/', loginRoute: '/login', // Optionally, specify some of the reserved routes
        cookies: {
            secure: false, // Set secure to true if the testing platform is in a different domain and https is being used
            sameSite: '' // Set sameSite to 'None' if the testing platform is in a different domain and https is being used
        },
        devMode: true, // Set DevMode to false if running in a production environment with https
        dynRegRoute: '/register', // Setting up dynamic registration route. Defaults to '/register'
        dynReg: {
            url: SERVER.href, // Tool Provider URL. Required field.
            name: 'MathALÉA', // Tool Provider name. Required field.
            logo: 'https://coopmaths.fr/alea/favicon.ico', // Tool Provider logo URL.
            description: 'Un exerciseur aléatoire mathématiques.', // Tool Provider description.
            redirectUris: [new URL('/launch', SERVER)], // Additional redirection URLs. The main URL is added by default.
            // customParameters: { tutu: 'rouge' }, // Custom parameters.
            autoActivate: true // Whether or not dynamically registered Platforms should be automatically activated. Defaults to false.
        },
        staticPath: path.join(__dirname, '/public'), // Path to static files
    }
)

// Set lti launch callback
lti.onConnect((token, req, res) => {
    console.log(token)
    return res.send('It\'s alive!')
})

// Deep Linking callback
lti.onDeepLinking((token, req, res) => {
    // Call redirect function to deep linking view
    lti.redirect(res, '/deeplink')
})

// Deep Linking route, displays the resource selection view
lti.app.get('/deeplink', async (req, res) => {
    return res.sendFile(path.join(__dirname, '/public/resources.html'))
})

lti.app.get('/launch', async (req, res) => {
    if (res.locals.token.userInfo && Object.keys(res.locals.token.userInfo).length) {
        res.locals.token.userInfo = {}
        return res.send('NO DATA !!!!')
    }
    return res.sendFile(path.join(__dirname, '/public/launch.html'))
});

lti.app.post('/grade', async (req, res) => {
    try {
        const idtoken = res.locals.token // IdToken
        const score = parseInt(req.body.grade) // User numeric score sent in the body
        // Creating Grade object
        const gradeObj = {
            userId: idtoken.user,
            scoreGiven: score,
            comment: "This is exceptional work.",
            scoreMaximum: 100,
            activityProgress: 'Completed',
            gradingProgress: 'FullyGraded'
        }

        // Selecting linetItem ID
        let lineItemId = idtoken.platformContext.endpoint.lineitem // Attempting to retrieve it from idtoken
        if (!lineItemId) {
            const response = await lti.Grade.getLineItems(idtoken, { resourceLinkId: true })
            const lineItems = response.lineItems
            if (lineItems.length === 0) {
                // Creating line item if there is none
                console.log('Creating new line item')
                const newLineItem = {
                    scoreMaximum: 100,
                    label: 'Grade',
                    tag: 'grade',
                    resourceLinkId: idtoken.platformContext.resource.id
                }
                const lineItem = await lti.Grade.createLineItem(idtoken, newLineItem)
                lineItemId = lineItem.id
            } else lineItemId = lineItems[0].id
        }

        // Sending Grade
        const responseGrade = await lti.Grade.submitScore(idtoken, lineItemId, gradeObj)
        return res.send(responseGrade)
    } catch (err) {
        return res.status(500).send({ err: err.message })
    }
})

lti.app.post('/deeplink', async (req, res) => {
    const resource = req.body

    const items = [
        {
            type: 'ltiResourceLink',
            title: resource.title,
            url: resource.url,
            /*custom: {
                tutu: 'rose'
            }*/
        }
    ]

    // Creates the deep linking request form
    const form = await lti.DeepLinking.createDeepLinkingForm(res.locals.token, items, { message: 'Successfully registered resource!' })

    return res.send(form)
})

const setup = async () => {
    // Deploy server and open connection to the database
    await lti.deploy({ port: 80 }) // Specifying port. Defaults to 3000
	
	  // Register platform
//   await lti.registerPlatform({
//     url: 'https://sandbox.moodledemo.net',
//     name: 'Platform Name',
//     clientId: 'nYt5BIIHIXX0EFX',
//     authenticationEndpoint: 'https://sandbox.moodledemo.net/mod/lti/auth.php',
//     accesstokenEndpoint: 'https://sandbox.moodledemo.net/mod/lti/token.php',
//     authConfig: { method: 'JWK_SET', key: 'https://sandbox.moodledemo.net/mod/lti/certs.php' }
//   })

}

setup()